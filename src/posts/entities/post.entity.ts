import { 
    Entity,
    Column, 
    PrimaryGeneratedColumn 
 } from 'typeorm'

@Entity()
export class PostEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column({ 
        name: 'title', 
        length: 100
    })
    title : string

    @Column({
        name: 'text',
        length: 400
    })
    text : string

    @Column({
        name: 'imagePath',
        nullable: true
    })
    imagePath : string




}