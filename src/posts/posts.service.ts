import { Injectable, Post } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { PostDto } from './dto/post.dto'
import { PostEntity } from './entities/post.entity'


@Injectable()
export class PostsService {

  constructor(
    @InjectRepository(PostEntity)
    private readonly postRepository : Repository<PostEntity>
  ) {}

  async findAll() : Promise<PostEntity[]> {
    return await this.postRepository.find()
  }

  async findById(id : number) : Promise<PostEntity> {
    return await this.postRepository.findOne(id)
  }

  async createPost(post : PostDto) {
    return this.postRepository.insert(post)
  }

  async index () {
    return {
      name: 'John Doe',
      age: 30
    }
  }
}
