import { Controller, Get, Query, Param, Post, Body } from '@nestjs/common'
import { PostsService } from './posts.service'
import { PostDto } from './dto/post.dto'

@Controller('posts')
export class PostsController {
  constructor (private readonly service: PostsService) {}

  @Get('/')
  async findAll () {
    return await this.service.findAll()
  }

  @Get('/:id')
  async find (@Param('id') id) {
    return await this.service.findById(id)
  }

  @Post()
  async create (@Body() post : PostDto) {
    console.log(post)
    return this.service.createPost(post)
  }
}
