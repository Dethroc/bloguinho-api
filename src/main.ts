import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  app.enableCors({
    methods : ['GET', 'POST'],
    origin : '*'
  })
  // app.use((req,res,next)=> {
  //   res.header('Access-Control-Allow-Origin', '*');
  //   res.header('Access-Control-Allow-Methods', 'GET,POST');
  //   res.header('Access-Control-Allow-Headers', 'Content-Type','Accept')
  //   next()
  // })
  await app.listen(3000)
}
bootstrap()
