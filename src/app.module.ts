import { Module } from '@nestjs/common'
import { PostsModule } from './posts/posts.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Connection } from 'typeorm'
import { PostEntity } from './posts/entities/post.entity'

@Module({
  imports: [
    TypeOrmModule.forRoot({
    type: 'sqlite',
    database: 'bloguinho.db',
    synchronize: true,
    logging: false,
    entities: [PostEntity],
  }), 
  PostsModule],
})
export class AppModule {
    constructor(private readonly connection: Connection) {}
  

}
